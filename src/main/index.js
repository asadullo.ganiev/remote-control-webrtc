import { app, BrowserWindow, ipcMain } from 'electron';
const robot = require('robotjs');
const url = require('url');
const path = require('path');

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path')
    .join(__dirname, '/static')
    .replace(/\\/g, '\\\\');
}

let mainWindow, roundWindow;
const winURL =
  process.env.NODE_ENV === 'development'
    ? `http://localhost:9080`
    : `file://${__dirname}/index.html`;

const roundWinURL =
  process.env.NODE_ENV === 'development'
    ? `http://localhost:9080/#/extra`
    : `file://${__dirname}/index.html#extra`;

function createWindow() {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 320,
    // useContentSize: true,
    width: 300,
    x: -3,
    y: 530,
    // transparent: false,
    transparent: true,
    frame: false,
    skipTaskbar: true,
    alwaysOnTop: true,
    // resizable: false,
    // fullscreenable: false,
    // maximizable: false,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true
    }
    // transparent: true
  });

  mainWindow.loadURL(winURL);

  if (process.env.NODE_ENV === 'development') {
    mainWindow.webContents.on('did-frame-finish-load', () => {
      mainWindow.webContents.once('devtools-opened', () => {
        mainWindow.focus();
      });
      // mainWindow.webContents.openDevTools();
    });
  }
  mainWindow.on('closed', () => {
    mainWindow = null;
    roundWindow = null;
  });

  roundWindow = new BrowserWindow({
    height: 520,
    // useContentSize: true,
    width: 400,
    // transparent: true,
    frame: false,
    // resizable: false,
    // fullscreenable: false,
    // maximizable: false,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true
    }
    // transparent: true
  });
  roundWindow.loadURL(roundWinURL);
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

ipcMain.on('key-press', (e) => {
  robot.keyTap('down');
});

ipcMain.on('minimize-window', (e) => {
  mainWindow.minimize();
});

ipcMain.on('close-window', (e) => {
  mainWindow.close();
  // roundWindow.close();
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
