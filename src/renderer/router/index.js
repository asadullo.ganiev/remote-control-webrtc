import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPage').default
    },
    {
      path: '/extra',
      name: 'roundVideo',
      component: require('@/components/helloWorld.vue').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
});
